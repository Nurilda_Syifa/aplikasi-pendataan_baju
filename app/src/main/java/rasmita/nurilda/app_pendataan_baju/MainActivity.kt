package rasmita.nurilda.app_pendataan_baju

import android.database.sqlite.SQLiteDatabase
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*



class MainActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {
    lateinit var db: SQLiteDatabase
    lateinit var fragDataBaju: FragmentDataBaju
    lateinit var ft: FragmentTransaction

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bottomNavigationView.setOnNavigationItemSelectedListener(this)
        fragDataBaju = FragmentDataBaju()

        db = DBOpenHelper(this).writableDatabase
    }

    fun getDbObject(): SQLiteDatabase{
        return db
    }

    override fun onNavigationItemSelected(p0: MenuItem): Boolean {
        when(p0.itemId){
            R.id.itemBaju ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout,fragDataBaju).commit()
                frameLayout.setBackgroundColor(Color.argb(245,255,255,255))
                frameLayout.visibility = View.VISIBLE
            }
            R.id.itemAbout -> frameLayout.visibility = View.GONE
        }
        return true
    }
}
