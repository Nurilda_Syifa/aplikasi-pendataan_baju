package rasmita.nurilda.app_pendataan_baju

import android.app.AlertDialog
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.CursorAdapter
import android.widget.ListAdapter
import android.widget.SimpleCursorAdapter
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.frag_data_baju.*
import kotlinx.android.synthetic.main.frag_data_baju.view.*
import kotlinx.android.synthetic.main.frag_data_baju.view.spinner

class FragmentDataBaju : Fragment(), View.OnClickListener, AdapterView.OnItemSelectedListener{
    override fun onNothingSelected(parent: AdapterView<*>?) {
        spinner.setSelection(0,true)
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val c = spAdapter.getItem(position) as Cursor
        namaUkuran = c.getString(c.getColumnIndex("_id"))
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnDeleteBaju->{

            }
            R.id.btnUpdateBaju->{

            }
            R.id.btnInsertBaju->{
                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Apakah data yang dimasukkan sudah benar?")
                    .setPositiveButton("Ya",btnInsertDialog)
                    .setNegativeButton("Tidak", null)
                dialog.show()

            }
        }
    }

    lateinit var thisParent : MainActivity
    lateinit var lsAdapter: ListAdapter
    lateinit var spAdapter: SimpleCursorAdapter
    lateinit var dialog : AlertDialog.Builder
    lateinit var v : View
    var namaBaju : String=""
    var namaUkuran : String=""
    lateinit var db : SQLiteDatabase

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as MainActivity
        v = inflater.inflate(R.layout.frag_data_baju,container,false)
        db = thisParent.getDbObject()
        dialog = AlertDialog.Builder(thisParent)
        v.btnDeleteBaju.setOnClickListener(this)
        v.btnInsertBaju.setOnClickListener(this)
        v.btnUpdateBaju.setOnClickListener(this)
        v.spinner.onItemSelectedListener = this
        return v
    }

    override fun onStart() {
        super.onStart()
        showDataBaju("")
        showDataUkuran()
    }

    fun showDataBaju(namaBaju: String){
        var sql= ""
        if(!namaBaju.trim().equals("")){
            sql = " select b.kode as _id, b.nama_baju, b.stok, u.nama_ukuran from barang b, ukuran u " +
                     "where b.id_ukuran=u.id_ukuran and b.nama_baju like '%$namaBaju%'"
        }else{
            sql = "select b.kode as _id, b.nama_baju,  b.stok, u.nama_ukuranfrom barang b, ukuran u " +
                    "where b.id_ukuran=u.id_ukuran order by b.nama_baju asc"
        }
        val c : Cursor = db.rawQuery(sql,null)
        lsAdapter = SimpleCursorAdapter(thisParent,R.layout.item_data_baju,c,
            arrayOf("_id","nama_baju","stok","nama_ukuran"), intArrayOf(R.id.txKodeBaju, R.id.txNamaBaju, R.id.txStok, R.id.txUkuran),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        v.lsBaju.adapter = lsAdapter

    }

    fun showDataUkuran(){
        val c : Cursor = db.rawQuery("select nama_ukuran as _id from ukuran order by nama_ukuran asc",null)
        spAdapter = SimpleCursorAdapter(thisParent, android.R.layout.simple_spinner_item,c,
        arrayOf("_id"), intArrayOf(android.R.id.text1),CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        spAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        v.spinner.adapter = spAdapter
        v.spinner.setSelection(0)
    }

    fun insertDataBaju(kode: String, namaBarang: String, stok : String, id_ukuran: Int){
        var sql = "insert into barang(kode, nama, stok, id_ukuran) values (?,?,?,?)"
        db.execSQL(sql, arrayOf(kode,namaBarang,id_ukuran,stok))
        showDataBaju("")
    }

    val btnInsertDialog = DialogInterface.OnClickListener { dialog, which ->
        var sql = "select id_ukuran from ukuran where nama_ukuran = '$namaUkuran'"
        var c : Cursor = db.rawQuery(sql,null)
        if(c.count>0){
            insertDataBaju(v.edKodeBaju.text.toString(), v.edNamaBaju.text.toString(), v.edStok.text.toString(),
            c.getInt(c.getColumnIndex("id_ukuran")))
            v.edKodeBaju.setText("")
            v.edNamaBaju.setText("")
            v.edStok.setText("")
        }
    }
}