package rasmita.nurilda.app_pendataan_baju

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBOpenHelper(context : Context):SQLiteOpenHelper(context,DB_Name, null,DB_Ver) {
    companion object {
        val DB_Name =  "toko"
        val DB_Ver = 1
    }

    override fun onCreate(db: SQLiteDatabase?) {
        val tBaju = "create table baju(kode text primary key, nama_baju text not null, id_ukuran int not null, stok text not null)"
        val tUkuran = "create table ukuran(id_ukuran integer primary key autoincrement, nama_ukuran text not null)"
        val insKategori = "insert into ukuran(nama_ukuran) values('M'),('S'),('L'),('XL'),('XXL'),('All Size')"
        db?.execSQL(tBaju)
        db?.execSQL(tUkuran)
        db?.execSQL(insKategori)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        TODO("Not yet implemented")
    }
}